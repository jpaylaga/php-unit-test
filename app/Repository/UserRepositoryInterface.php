<?php

namespace DTApi\Repository;

interface UserRepositoryInterface
{
	public function createOrUpdate($id = null, $request, array $roles = []);
	public function enable($id);
	public function disable($id);
	public function getTranslators();
}