<?php

namespace DTApi\Repository\RoleObjects;

use DTApi\Models\User;
use DTApi\Models\UserMeta;

interface RoleObject
{
	public function role();
	public function createOrUpdate(User $model, UserMeta $user_meta, $request);
}