<?php

namespace DTApi\Repository\RoleObjects;

use DTApi\Models\UserLanguages;

use DTApi\Models\User;
use DTApi\Models\UserMeta;

class Translator implements RoleObject
{
	public function role()
	{
		return env('TRANSLATOR_ROLE_ID');
	}

	public function createOrUpdate(User $model, UserMeta $user_meta, $request)
	{
		$user_meta->translator_type = $request['translator_type'];
        $user_meta->worked_for = $request['worked_for'];
        if ($request['worked_for'] == 'yes') {
        	$user_meta->organization_number = $request['organization_number'];
        }
        $user_meta->gender = $request['gender'];
        $user_meta->translator_level = $request['translator_level'];
        $user_meta->address_2 = $request['address_2'];
        $user_meta->save();

        $data['translator_type'] = $request['translator_type'];
        $data['worked_for'] = $request['worked_for'];
        if ($request['worked_for'] == 'yes') {
        	$data['organization_number'] = $request['organization_number'];
        }
        $data['gender'] = $request['gender'];
        $data['translator_level'] = $request['translator_level'];

        $langidUpdated = [];
        if ($request['user_language']) {
        	foreach ($request['user_language'] as $langId) {
            	$userLang = new UserLanguages();
                $already_exit = $userLang::langExist($model->id, $langId);
                if ($already_exit == 0) {
                	$userLang->user_id = $model->id;
                    $userLang->lang_id = $langId;
                    $userLang->save();
                }
                $langidUpdated[] = $langId;

            }
            if ($langidUpdated) {
            	$userLang::deleteLang($model->id, $langidUpdated);
            }
        }
	}
}