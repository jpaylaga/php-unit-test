<?php

namespace DTApi\Repository\RoleObjects;

use DTApi\Models\Company;
use DTApi\Models\Department;
use DTApi\Models\Type;
use DTApi\Models\UsersBlacklist;

use DTApi\Models\User;
use DTApi\Models\UserMeta;

class Customer implements RoleObject
{
	public function role()
	{
		return env('CUSTOMER_ROLE_ID');
	}

	public function createOrUpdate(User $model, UserMeta $user_meta, $request)
	{
		if($request['consumer_type'] == 'paid') {
            if($request['company_id'] == '') {
                $type = Type::where('code', 'paid')->first();
                $company = Company::create(['name' => $request['name'], 'type_id' => $type->id, 'additional_info' => 'Created automatically for user ' . $model->id]);
                $department = Department::create(['name' => $request['name'], 'company_id' => $company->id, 'additional_info' => 'Created automatically for user ' . $model->id]);

                $model->company_id = $company->id;
                $model->department_id = $department->id;
                $model->save();
            }
        }

        $user_meta->consumer_type = $request['consumer_type'];
        $user_meta->customer_type = $request['customer_type'];
        $user_meta->username = $request['username'];
        $user_meta->city = $request['city'];
        $user_meta->country = $request['country'];
        $user_meta->reference = (isset($request['reference']) && $request['reference'] == 'yes') ? '1' : '0';
        $user_meta->cost_place = isset($request['cost_place']) ? $request['cost_place'] : '';
        $user_meta->fee = isset($request['fee']) ? $request['fee'] : '';
        $user_meta->time_to_charge = isset($request['time_to_charge']) ? $request['time_to_charge'] : '';
        $user_meta->time_to_pay = isset($request['time_to_pay']) ? $request['time_to_pay'] : '';
        $user_meta->charge_ob = isset($request['charge_ob']) ? $request['charge_ob'] : '';
        $user_meta->customer_id = isset($request['customer_id']) ? $request['customer_id'] : '';
        $user_meta->charge_km = isset($request['charge_km']) ? $request['charge_km'] : '';
        $user_meta->maximum_km = isset($request['maximum_km']) ? $request['maximum_km'] : '';
        $user_meta->save();

        $blacklistUpdated = [];
        $userBlacklist = UsersBlacklist::where('user_id', $id)->get();
        $userTranslId = collect($userBlacklist)->pluck('translator_id')->all();

        $diff = null;
        if ($request['translator_ex']) {
        	$diff = array_intersect($userTranslId, $request['translator_ex']);
        }
        if ($diff || $request['translator_ex']) {
        	foreach ($request['translator_ex'] as $translatorId) {
            	$blacklist = new UsersBlacklist();
                if ($model->id) {
                	$already_exist = UsersBlacklist::translatorExist($model->id, $translatorId);
                    if ($already_exist == 0) {
                    	$blacklist->user_id = $model->id;
                        $blacklist->translator_id = $translatorId;
                        $blacklist->save();
                    }
                    $blacklistUpdated [] = $translatorId;
                }
			}
            if ($blacklistUpdated) {
            	UsersBlacklist::deleteFromBlacklist($model->id, $blacklistUpdated);
            }
        } else {
        	UsersBlacklist::where('user_id', $model->id)->delete();
        }
	}
}