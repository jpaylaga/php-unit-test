<?php

namespace DTApi\Providers;

use Illuminate\Support\ServiceProvider;
use DTApi\Repository\UserRepositoryInterface;
use DTApi\Repository\UserRepository;

class ServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UserRepositoryInterface::class, function() {
            return new UserRepository(auth()->user());
        });
    }
}
