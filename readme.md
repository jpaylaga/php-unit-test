﻿# Customers and Translators App

## My Observations:

Based on the code that was given, I believe that it is centered along the Repository Pattern.  If implemented properly, it greatly helps in organizing different CRUD logic and decouples it away from the controller.

There are many cases that decoupling CRUD or any database logic from the controller could really be useful, such as being able to transition from one database engine to another seemlessly and being able to use the same CRUD logic into different controllers, for example, you have web controllers and api controllers with the same logic. That is my main goal in writing repository pattern. The 2nd priority would be the capability to transition from one database engine to another seemlessly as it is especially useful when you have raw database queries written.

The code that I was tasked to refactor was already good enough yet I think it lacks some elements that will make it more suitable in the scenario that the same set of logic needs to have another implementation, hence, I added an interface.  In my refactoring, I added `UserRepositoryInterface` so that controllers will be able to refer its abtract definition only instead of referring to its concrete implementation. Its concrete implementation will be binded to it via Laravel's Service Provider.  I have implemented a singleton binding for any case that it will be instantiated with the current authenticated user as a dependency as it ensures that only one instance will be created.

In the `UserRepository::createOrUpdate()`, I refactored its implementation to decouple its process of creating or updating a user. I decoupled the part where updating the `UserMeta` is based on its role.  The problem that is solves is when the system will have more than 2 roles and when the user role becomes dynamic.  The objects implementing `RoleObject` also is flexible enough that it can be reused in other logic as well relating to user role.  Corresponding unit test also has been updated.

Depends on the context of the system, if I will be developing a system, I would generally be writing a repository pattern that is not tied to a specific model (Evident by supplying a model as DI) as in most cases a controller may require output or value that is derived from 2 or more unrelated models. Also I would be adding a service layer to contain non-database specific logics and should contain only business logic.
