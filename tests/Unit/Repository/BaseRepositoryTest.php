<?php

namespace Tests\Unit\Repository;

use DTApi\Repository\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BaseRepositoryTest extends TestCase
{
    private $repo;

    public function setUp(): void
    {
        parent::setUp();
        $model = \Mockery::mock(Model::class);
        $model->shouldReceive('all')->andReturns(new Collection());
        $model->shouldReceive('find')->andReturns(\Mockery::mock(Model::class));
        $model->shouldReceive('findOrFail')->with(1)->andReturns(\Mockery::mock(Model::class));
        $model->shouldReceive('findOrFail')->with(2)->andThrow(new ModelNotFoundException());
        $model->shouldReceive('where->first')->andReturns(\Mockery::mock(Model::class));
        $model->shouldReceive('query')->andReturns(\Mockery::mock(Builder::class));
        $model->shouldReceive('instance')->andReturns(\Mockery::mock(Model::class));

        $this->repo = new BaseRepository($model);
    }

    public function tearDown(): void
    {
        parent::tearDown();
        \Mockery::close();
    }

    public function testInstantiation()
    {
        // Makes sure no error when instantiating with correct DI.
        $model = \Mockery::mock(Model::class);
        $repo1 = new BaseRepository($model);

        // Should trigger an error if supplied with incorect class
        $hasError = false;
        try {
            $repo2 = new BaseRepository(new \stdClass());
        } catch (\Throwable $e) {
            $hasError = true;
        }

        $this->assertTrue($hasError);
    }

    public function testValidatorAttributeNames()
    {
        $this->assertIsArray($this->repo->validatorAttributeNames());
    }

    public function testGetModel()
    {
        $this->assertInstanceOf(Model::class, $this->repo->getModel());
    }

    public function testAll()
    {
        $this->assertInstanceOf(Collection::class, $this->repo->all());
    }

    public function testFind()
    {
        $model1 = $this->repo->find(1);
        $this->assertInstanceOf(Model::class, $model1);
    }

    public function testFindOrFail()
    {
        $this->assertInstanceOf(Model::class, $this->repo->findOrFail(1));
        $hasException = false;
        try {
            $this->repo->findOrFail(2);
        } catch (\Exception $e) {
            $hasException = true;
        }
        $this->assertTrue($hasException);
    }

    public function testFindBySlug()
    {
        $this->assertInstanceOf(Model::class, $this->repo->findBySlug('test'));
    }

    public function testQuery()
    {
        $this->assertInstanceOf(Builder::class, $this->repo->query());
    }

    /**
     * Problem with implementation
     */
    /*public function testInstance()
    {
        $this->assertInstanceOf(Model::class, $this->repo->instance([
            'test' => 'test',
        ]));
    }*/
}
