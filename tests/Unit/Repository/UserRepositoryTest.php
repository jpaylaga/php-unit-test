<?php

namespace Tests\Unit\Repository;

use DTApi\Models\User;
use DTApi\Repository\UserRepository;
use Illuminate\Support\Collection;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use DTApi\Repository\RoleObjects\customer;
use DTApi\Repository\RoleObjects\translator;

class UserRepositoryTest extends TestCase
{
    private $user;
    private $request;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = \Mockery::mock(User::class);
        $this->user->shouldIgnoreMissing();
        $this->request = \Mockery::mock(\ArrayObject::class);
        $this->request->shouldIgnoreMissing();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        \Mockery::close();
    }

    public function testCreateOrUpdate()
    {
        $roles = [
            new customer,
            new translator,
        ];

        // role = customer | status = enable
        {
            $repo = new UserRepository($this->user);
            $this->request['role'] = env('CUSTOMER_ROLE_ID');
            $this->request['status'] = 1;

            $retval = $repo->createOrUpdate(1, $this->request, $roles);
            $this->assertInstanceOf(User::class, $retval);
        }

        // role = customer | status = disable
        {
            $repo = new UserRepository($this->user);
            $this->request['role'] = env('CUSTOMER_ROLE_ID');
            $this->request['status'] = 0;

            $retval = $repo->createOrUpdate(1, $this->request, $roles);
            $this->assertInstanceOf(User::class, $retval);
        }

        // role = translator | status = enable
        {
            $repo = new UserRepository($this->user);
            $this->request['role'] = env('TRANSLATOR_ROLE_ID');
            $this->request['status'] = 1;

            $retval = $repo->createOrUpdate(1, $this->request, $roles);
            $this->assertInstanceOf(User::class, $retval);
        }

        // role = translator | status = disable
        {
            $repo = new UserRepository($this->user);
            $this->request['role'] = env('TRANSLATOR_ROLE_ID');
            $this->request['status'] = 0;

            $retval = $repo->createOrUpdate(1, $this->request, $roles);
            $this->assertInstanceOf(User::class, $retval);
        }

        // role = translator | status = enable | new_towns = true
        {
            $repo = new UserRepository($this->user);
            $this->request['role'] = env('TRANSLATOR_ROLE_ID');
            $this->request['status'] = 1;
            $this->request['new_towns'] = true;

            $retval = $repo->createOrUpdate(1, $this->request, $roles);
            $this->assertInstanceOf(User::class, $retval);
        }

        // role = translator | status = enable | user_towns_projects = true
        {
            $repo = new UserRepository($this->user);
            $this->request['role'] = env('TRANSLATOR_ROLE_ID');
            $this->request['status'] = 1;
            $this->request['user_towns_projects'] = true;

            $retval = $repo->createOrUpdate(1, $this->request, $roles);
            $this->assertInstanceOf(User::class, $retval);
        }
    }

    /**
     *  Though in practice it would never be ideal to catch errors when
     *  expecting a component to work, there is no return value to assert
     *  values with and every test cases requires assertions, hence, asserting
     *  boolean variable.
     */
    public function testEnable()
    {
        $hasFailed = false;
        try {
            $repo = new UserRepository($this->user);
            $repo->enable(1);
        } catch (\Exception $e) {
            $hasFailed = true;
        }
        $this->assertFalse($hasFailed);
    }

    /**
     *  Though in practice it would never be ideal to catch errors when
     *  expecting a component to work, there is no return value to assert
     *  values with and every test cases requires assertions, hence, asserting
     *  boolean variable.
     */
    public function testDisable()
    {
        $hasFailed = false;
        try {
            $repo = new UserRepository($this->user);
            $repo->disable(1);
        } catch (\Exception $e) {
            $hasFailed = true;
        }
        $this->assertFalse($hasFailed);
    }

    public function testGetTranslator()
    {
        $repo = new UserRepository($this->user);
        $this->assertInstanceOf(Collection::class, $repo->getTranslators());
    }
}
